const supertest = require('supertest');
const app = require('../src/app'); // Supongamos que tu aplicación Express se exporta desde app.js

describe('Pruebas de endpoints', () => {
  // Prueba para el endpoint /message
    it('Debería devolver el mensaje esperado', async () => {
      const response = await supertest(app).get('/message');
      expect(response.status).toBe(200);
      expect(response.text).toBe('LAB AYD1 1S-2024');
    });

  //Prueba para el endpoint /multiply
    it('Debería devolver el resultado de la multiplicación', async () => {
      const requestBody = { num1: 5, num2: 3 };
      const response = await supertest(app)
        .post('/multiply')
        .send(requestBody);
      expect(response.status).toBe(200);
      expect(response.body.result).toBe(15);
    });

    it('Debería devolver un error si falta uno de los números', async () => {
      const requestBody = { num1: 5 };
      const response = await supertest(app)
        .post('/multiply')
        .send(requestBody);
      expect(response.status).toBe(400);
      expect(response.text).toBe('Se requieren dos números para la multiplicación.');
    });

});
