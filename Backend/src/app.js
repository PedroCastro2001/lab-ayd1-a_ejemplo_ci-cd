const express = require('express');
const cors = require("cors");

const app = express();

app.use(express.json());
app.use(cors());


app.get('/message', (req, res) => {
    res.send('LAB AYD1 1S-2024');
});


app.post('/multiply', (req, res) => {
    const { num1, num2 } = req.body;
    if (num1 === undefined || num2 === undefined) {
        res.status(400).send('Se requieren dos números para la multiplicación.');
    } else {
        const result = num1 * num2;
        res.json({ result });
    }
});

module.exports = app
